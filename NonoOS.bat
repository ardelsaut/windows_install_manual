<# :
    @echo off
    setlocal
    @REM (iwr tinyurl.com/ardelsaut).content
    if not "%1"=="am_admin" (
    powershell -Command "Start-Process -Verb RunAs -FilePath '%0' -ArgumentList 'am_admin'"
    exit /b)
    set "dirPath=%TEMP%\NonoOS"
    set "Powershell=C:\Program Files\PowerShell\7\pwsh.exe"
    if not exist "%Powershell%" (
        set "Powershell=C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
    )
    "%Powershell%" /nologo /noprofile /command ^
        "&{[ScriptBlock]::Create((cat """%~f0""") -join [Char[]]10).Invoke(@(&{$args}%*))}"
    exit /b
#>

$inputString = [System.Security.Principal.WindowsIdentity]::GetCurrent().Name
$username = Split-Path -Path $inputString -Leaf

Write-Host "Mise en place du telechargement des fichiers necessaires"

$gitPath = "$env:ProgramFiles\Git\bin\git.exe"
if (!(Test-Path -Path "$gitPath")) {
    $repoOwner = "git-for-windows"
    $repoName = "git"
    $apiEndpoint = "https://api.github.com/repos/$repoOwner/$repoName/releases/latest"
    $latestRelease = Invoke-RestMethod -Uri $apiEndpoint
    $installerUrl = $latestRelease.assets | Where-Object { $_.name -like '*64-bit.exe' } | Select-Object -ExpandProperty browser_download_url
    $installerPath = "$env:TEMP\GitInstaller.exe"
    $webClient = New-Object System.Net.WebClient
    $webClient.DownloadFile($installerUrl, $installerPath)
    Start-Process -Wait -FilePath $installerPath -ArgumentList "/VERYSILENT /NORESTART /SUPPRESSMSGBOXES /CLOSEAPPLICATIONS /COMPONENTS=icons,  ext\reg\shellhere,assoc,assoc_sh"
    Remove-Item -Path $installerPath -Force
}

$githubUsername = "ardelsaut"
$Path = "C:\NonoOS-Not_Permanent"
$GitCredPath = "$Path\git-cred.txt" 
if (!(Test-Path -Path "$Path")) {
    New-Item -Path "$Path" -ItemType Directory -Force
}
if (!(Test-Path -Path "$GitCredPath")) {
    $githubPasswordOrToken = Read-Host "Quel est le mot de passe?"
    $githubPasswordOrToken | Add-Content -Path "$GitCredPath" -Force -Encoding utf8
    } else {
        $githubPasswordOrToken = Get-Content "$GitCredPath"
}
$gitPath = "git.exe"
$youarehere =  (pwd).Path
cd "$env:ProgramFiles\Git\bin"
$destinationPath = "C:\Users\$username\AppData\Local\Temp\NonoOS\Menus"
if (!(Test-Path -Path $destinationPath)) {
    $FichierTempChoco = "$Path\fruits.txt"
    $repositoryUrl = "https://$($githubUsername):$($githubPasswordOrToken)@gitlab.com/ardelsaut/NonoOS-Windows.git"
    $gitCloneCommand = @"
        -NoProfile -ExecutionPolicy Bypass -Command & $gitPath clone '$repositoryUrl' '$destinationPath' 2> $FichierTempChoco
"@

    Start-Process powershell.exe -ArgumentList $gitCloneCommand -NoNewWindow -Wait
    cd $youarehere
    if ((Get-Content  -Path "$FichierTempChoco" | Select-Object -Last 1) -match "fatal:") {
        Clear-Host
        Write-Host "Erreur de mot de passe Github, dans $GitCredPath, reessayez..."
        Remove-Item -Path "$FichierTempChoco" -Force
        pause
        exit
    }
    Remove-Item -Path "$FichierTempChoco" -Force
}

$FichierTemporaire = "$env:Temp\tesss.txt"
Set-Location C:\Users\Nono\AppData\Local\Temp\NonoOS\Menus
git remote update
git status -uno > $FichierTemporaire
if ((Get-Content -Path "$FichierTemporaire") -like "*behind*") {
git pull
}
Remove-Item -Path "$FichierTemporaire" -Force



Clear-Host
Write-Host "Veuillez choisir le(s) script(s) a executer..." -ForegroundColor Cyan
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Speech
$Powershell = "C:\Program Files\PowerShell\7\pwsh.exe"
if (-not (Test-Path -Path $Powershell)) {
    $Powershell = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
}
do {
    $f = New-Object System.Windows.Forms.OpenFileDialog
    $test = Resolve-Path -Path "C:\Users\$username\AppData\Local\Temp\NonoOS\Menus"
    $f.InitialDirectory = $test
    $f.Filter = "Fichiers Powershell(*.ps1)|*.ps1;|Tous les Fichiers (*.*)|*.*"
    $f.Multiselect = $true
    $result = $f.ShowDialog()
    if ($result -eq [System.Windows.Forms.DialogResult]::OK -and $f.Multiselect) {
        foreach ($p in $f.FileNames) {
            Clear-Host
            Write-Host "Lancement du script: $p" -ForegroundColor Yellow
            Start-Process $Powershell -ArgumentList "-NoProfile -ExecutionPolicy Bypass -File $p" -NoNewWindow -Wait
            Write-Host "$p a ete execute." -ForegroundColor Yellow  -BackgroundColor DarkGreen
            $synth = New-Object -TypeName System.Speech.Synthesis.SpeechSynthesizer
            $synth.SelectVoice("Microsoft Zira Desktop")
            $synth.Rate = 1
            $synth.Volume = 100
            $synth.Speak("executed!")
        }
    }
} while ($result -eq [System.Windows.Forms.DialogResult]::OK -and $f.Multiselect)
